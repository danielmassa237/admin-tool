import { NgModule, ModuleWithProviders } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MaterialModule } from '../material';
import { LayoutComponent } from './layout';
import { ToolbarComponent } from './toolbar';
import { NotFoundPageComponent } from './not-found-page';
import { StartPageComponent } from './start-page';

export const COMPONENTS = [
  LayoutComponent,
  NotFoundPageComponent,
  ToolbarComponent,
  StartPageComponent
];

@NgModule({
  imports: [MaterialModule, RouterModule],
  declarations: COMPONENTS,
  exports: COMPONENTS
})
export class CoreModule {

  static forRoot(): ModuleWithProviders {
    return {
      ngModule: CoreModule,
      providers: []
    };
  }
}
