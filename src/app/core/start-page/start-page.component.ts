import { Component } from '@angular/core';

@Component({
  selector: 'dm-start-page',
  templateUrl: './start-page.component.html',
  styleUrls: ['./start-page.component.scss']
})
export class StartPageComponent {
  title = 'app';
}
