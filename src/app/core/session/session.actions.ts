import { Action } from '@ngrx/store';

import { SessionResponse } from './session.models';

export const VERIFY = '[Session] Verify';
export const VERIFY_SUCCESS = '[Session] Verify Success';
export const VERIFY_FAILURE = '[Session] Verify Failure';

export class Verify implements Action {
  readonly type = VERIFY;

  constructor(public token: string) { }
}

export class VerifySuccess implements Action {
  readonly type = VERIFY_SUCCESS;

  constructor(public payload: SessionResponse) { }
}

export class VerifyFailure implements Action {
  readonly type = VERIFY_FAILURE;

  constructor(public payload: string) { }
}
