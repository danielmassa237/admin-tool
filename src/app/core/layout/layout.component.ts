import { Component, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'dm-layout',
  templateUrl: './layout.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styles: [],
})
export class LayoutComponent {}
