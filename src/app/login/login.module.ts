import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule, MatInputModule, MatCardModule, MatToolbarModule, MatButtonModule } from '@angular/material';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { MaterialModule } from '../material';
import { LoginFormComponent } from './login-form/login-form.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { AuthService } from './auth/auth.service';
import { AuthEffects } from './auth/auth.effects';
import { reducers, LOGIN_FEATURE_NAME } from './login.selector';

export const COMPONENTS = [LoginFormComponent, LoginPageComponent];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MaterialModule
  ],
  declarations: COMPONENTS,
  exports: COMPONENTS,
})
export class LoginModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: RootAuthModule,
      providers: [AuthService]
    };
  }
}

@NgModule({
  imports: [
    LoginModule,
    StoreModule.forFeature(LOGIN_FEATURE_NAME, reducers),
    EffectsModule.forFeature([AuthEffects])
  ],
  exports: COMPONENTS
})
export class RootAuthModule { }
