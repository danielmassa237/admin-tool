import * as auth from '../auth/auth.actions';

export interface State {
  error: string | null;
  busy: boolean;
}

export const initialState: State = {
  error: null,
  busy: false
};

export const reducer = (state = initialState, action: auth.Actions): State => {
  switch (action.type) {
    case auth.LOGIN: {
      return {
        ...state,
        error: null,
        busy: true
      };
    }
    case auth.LOGIN_SUCCESS: {
      return {
        ...state,
        error: null,
        busy: false
      };
    }
    case auth.LOGIN_FAILURE: {
      return {
        ...state,
        error: action.payload,
        busy: false
      };
    }
    default: {
      return state;
    }
  }

};

export const getError = (state: State) => state.error;
export const isBusy = (state: State) => state.busy;
