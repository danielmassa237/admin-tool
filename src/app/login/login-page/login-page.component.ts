import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

import { Authenticate } from '../auth/auth.models';
import { Login } from '../auth/auth.actions';
import * as fromLogin from '../login.selector';

@Component({
  selector: 'dm-login-page',
  templateUrl: 'login-page.component.html',
  styles: []
})
export class LoginPageComponent implements OnInit {

  isBusy$ = this.store.select(fromLogin.isBusy);
  error$ = this.store.select(fromLogin.getError);

  constructor(private store: Store<fromLogin.State>) { }

  ngOnInit(): void { }

  onSubmit($event: Authenticate) {
    this.store.dispatch(new Login($event));
  }

}
