import { createSelector, createFeatureSelector } from '@ngrx/store';
import * as fromAuth from './auth/auth.reducer';
import * as fromLoginPage from './login-page/login-page.reducer';
import { State as fromAppState } from '../app.reducer';

export interface State extends fromAppState {
  login: LoginState;
}

export interface LoginState {
  status: fromAuth.State;
  page: fromLoginPage.State;
}

export const reducers = {
  status: fromAuth.reducer,
  page: fromLoginPage.reducer
};

export const LOGIN_FEATURE_NAME = 'login';
export const selectLoginState = createFeatureSelector<LoginState>(LOGIN_FEATURE_NAME);

export const selectLoginStatusState = createSelector(selectLoginState, (state: LoginState) => state.status);
export const isLoggedInState = createSelector(selectLoginStatusState, fromAuth.isLoggedIn);
export const getSession = createSelector(selectLoginStatusState, fromAuth.getSession);

export const selectLoginPageState = createSelector(selectLoginState, (state: LoginState) => state.page);
export const isBusy = createSelector(selectLoginPageState, fromLoginPage.isBusy);
export const getError = createSelector(selectLoginPageState, fromLoginPage.getError);
