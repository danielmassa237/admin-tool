import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/exhaustMap';
import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { of } from 'rxjs/observable/of';

import * as Auth from './auth.actions';
import { AuthService } from './auth.service';

@Injectable()
export class AuthEffects {

  @Effect()
  login$ = this.actions$
    .ofType(Auth.LOGIN)
    .map((action: Auth.Login) => action.payload)
    .exhaustMap(auth =>
      this.service.login(auth)
        .map(session => new Auth.LoginSuccess(session))
        .catch(error => of(new Auth.LoginFailure(error)))
    );

  constructor(
    private actions$: Actions,
    private service: AuthService
  ) { }

}
