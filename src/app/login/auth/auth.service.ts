import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { _throw } from 'rxjs/observable/throw';

import { Authenticate, AuthenticateToken } from './auth.models';

export class AuthService {

  constructor() { }

  login({ email, password }: Authenticate): Observable<AuthenticateToken> {
    /**
     * Simulate a failed login to display the error
     * message for the login form.
     */
    if (email !== 'test@gmail.com') {
      return _throw('Invalid email or password');
    }

    return of({
      token: 'xxxxx.yyyyy.zzzzz',
      ttl: 5000
    });
  }
}
