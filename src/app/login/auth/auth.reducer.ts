import * as auth from './auth.actions';
import { AuthenticateToken } from './auth.models';

export interface State {
  loggedIn: boolean;
  session: AuthenticateToken;
}

export const initialState: State = {
  loggedIn: false,
  session: null
};

export const reducer = (state = initialState, action: auth.Actions) => {
  switch (action.type) {
    case auth.LOGIN_SUCCESS: {
      return {
        ...state,
        loggedIn: true,
        session: (action as auth.LoginSuccess).payload
      };
    }
    case auth.LOGOUT: {
      return initialState;
    }
    default: {
      return state;
    }
  }
};

export const isLoggedIn = (state: State) => state.loggedIn;
export const getSession = (state: State) => state.session;

