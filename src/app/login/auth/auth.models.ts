export interface Authenticate {
  email: string;
  password: string;
}

export interface AuthenticateToken {
  token: string;
  ttl: number;
}
