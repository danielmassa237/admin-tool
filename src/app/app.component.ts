import { Component, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'dm-app',
  template: `<router-outlet></router-outlet>`,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent {}
