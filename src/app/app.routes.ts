import { Routes } from '@angular/router';
import { StartPageComponent } from './core/start-page';
import { LoginPageComponent } from './login/login-page/login-page.component';
import { NotFoundPageComponent } from './core/not-found-page/not-found-page.component';

export const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  /*{
    path: '',
    component: StartPageComponent,
    canActivate: []
  },*/
  { path: 'login', component: LoginPageComponent },
  { path: '**', component: NotFoundPageComponent }
];
